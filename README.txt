Requirements:
------------
You need GCC 4.9 or higher (required support of C++11) to build 
the executable.


Building the executable:
-----------------------
> cd ds-vrptw/
> make clean && make
> cd ..

Testing:
-------
To optimize and simulate the execution of one operational day, 
on operational context OC-200-140-30%-1/7-11-11-7 and instance 
scenario OC-200-140-30%-1/7-11-11-7/instances/IN-O1.txt:

> ./ds-vrptw/bin/dsvrptw_gsa -t RIZZO -i benchmarks/OC-250-175-25%-1/12-19-0-0/ -o 10 --scenario 1 -s 5 -V van:1:3 -V bike:2:8 -D 0 -O current-sol.out -G --verbose

The two -V options indicate the vehicles available and to which 
depots they are assigned. -V van:1:3 (resp. bike:2:8) says 
that 3 (resp. 8) vehicles of type van (resp. bike) are assigned 
to depot number 1 (resp. 2). Finally, the -D 1 option indicates
to the solver that we want to use the dropboxes (-D 0 otherwise).

The -o, -s options say that the allowed offline computational 
time is 10s (-o 10) and that each time unit is worth 5s of 
experimental CPU time (i.e., since each time unit represents
one minute of real time, one minute of real time will be 
simulated during only 5 CPU seconds; set -s 60 to experiment 
under real-scale simulation). 

The -O option allows to follow, during the simulation, the state
of the current solution; it produces a current-sol.out file 
that can be dynamically viewed using the watch Unix command:
(the file is dynamically modified during the simulation and
optimization process, and contains ANSI color sequences)
> watch -t -n 0.5 --color cat current-sol.out


Error messages:
--------------

No initial feasible solution found ! Abording.
=> Offline computation failed at finding an initial feasible
   solution, that is one that successfully satisfies all the
   offline (deterministic) requests.
Solution: either augment the number of vehicles, or provide 
   a longer offline computation time (using the -o option)

Assertion `computation_time >= sec_per_time_unit / 10` failed
=> The solver is having hard time in applying the GSA decision
   rule provided the current number of CPU seconds per time 
   units that are simulated.
   This is likely to happen on very large instances, depending
   on the CPU performances. 
Solution: either augment the number of seconds per simulated
   time units (option -s), or decrease the scearion pool size 
   (option -p).
   In case of really hard instances, use the -G option (for 
   greedy evaluation, disables the GSA decision rule). 


