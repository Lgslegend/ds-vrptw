HOSTNAME := $(shell hostname | cut -b -4)


ifeq ($(HOSTNAME),mac-)
	export MAKEFLAGS = "-j $(getconf _NPROCESSORS_ONLN)"
	CC		= g++
	# CFLAGS	= -g -O0 -Wall -W -std=c++11 -DASSERTS
	CFLAGS	= -O3 -Wall -W -std=c++11 
else
	ifeq ($(HOSTNAME),cca0)	# CC-IN2P3 cluster
		GCCDIR = /afs/in2p3.fr/home/m/msaintgu/private/gcc-dev
	endif
	ifeq ($(HOSTNAME),clau)
		export MAKEFLAGS = "-j 4"
		GCCDIR = /etinfo/users2/stguillain/gcc-dev
	endif
	ifeq ($(HOSTNAME),drag)	# Dragon1 cluster
		export MAKEFLAGS = "-j $(getconf _NPROCESSORS_ONLN)"
		GCCDIR = /home/ucl/ingi/stguilla/gcc/gcc-dev-7.1.0
	endif
	ifeq ($(HOSTNAME),hmem) # HMEM cluster
		export MAKEFLAGS = "-j $(getconf _NPROCESSORS_ONLN)"	
		GCCDIR = /home/ucl/ingi/stguilla/gcc/gcc-dev-7.1.0
	endif
	ifeq ($(HOSTNAME),node)	# VEGA cluster
		export MAKEFLAGS = "-j $(getconf _NPROCESSORS_ONLN)"
		GCCDIR = /home/ucl/ingi/stguilla/gcc/gcc-dev-7.1.0
	endif
	ifeq ($(HOSTNAME),lema)	# LEMAITRE2 cluster
		export MAKEFLAGS = "-j $(getconf _NPROCESSORS_ONLN)"
		GCCDIR = /home/ucl/ingi/stguilla/gcc/gcc-dev-7.1.0
	endif

	CC		= $(GCCDIR)/bin/g++
	export LD_LIBRARY_PATH	= $(GCCDIR)/gcc-dev/lib:$(GCCDIR)/lib64
	export GCC_EXEC_PREFIX	= $(GCCDIR)/lib/gcc/
	# CFLAGS	= -pg -g -O3 -Wall -W -L/etinfo/users2/stguillain/gcc-dev/lib64 -I/etinfo/users2/stguillain/gcc-dev/include -B/etinfo/users2/stguillain/gcc-dev/lib/gcc/ -std=c++11	# -pg option for GCC is used only for profiler ! -g is for debugging !
	CFLAGS	= -O3 -Wall -W -L$(GCCDIR)/lib64 -I$(GCCDIR)/include -B$(GCCDIR)/lib/gcc/ -std=c++11
endif
LIBPATH = ../vrplib

SRC		= 	$(LIBPATH)/VRP_instance.cpp				\
			$(LIBPATH)/VRP_routes.cpp				\
			$(LIBPATH)/VRP_solution.cpp				\
			$(LIBPATH)/VRP_scenario.cpp				\
			$(LIBPATH)/readInstanceFiles.cpp

INCLUDES = -I$(LIBPATH)


SRC_PROGS = dsvrptw_test_instance.cpp dsvrptw_gsa.cpp dsvrptw_test_relocation_only.cpp test.cpp

PROGS	= 	dsvrptw_test_instance dsvrptw_gsa dsvrptw_test_relocation_only test


OBJ		= $(SRC:.cpp=.o)

all: $(PROGS) $(SRC) $(SRC_PROGS)

.cpp.o: 
	$(CC) $(CFLAGS) -c $< -o $@ $(INCLUDES) 

# $(PROGS): $(OBJ)
# 	$(CC) $(CFLAGS) $(OBJ) -o $@


test: $(OBJ) test.o
	mkdir -p bin
	$(CC) $(CFLAGS) $(OBJ) $@.o -o bin/$@


dsvrptw_test_relocation_only: $(OBJ) dsvrptw_test_relocation_only.o
	mkdir -p bin
	$(CC) $(CFLAGS) $(OBJ) $@.o -o bin/$@

dsvrptw_test_instance: $(OBJ) dsvrptw_test_instance.o
	mkdir -p bin
	$(CC) $(CFLAGS) $(OBJ) $@.o -o bin/$@
	
dsvrptw_gsa: $(OBJ) dsvrptw_gsa.o
	mkdir -p bin
	$(CC) $(CFLAGS) $(OBJ) $@.o -o bin/$@

clean:
	rm bin/* *.o $(LIBPATH)/*.o


.PHONY: clean