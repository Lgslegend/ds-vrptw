#include "VRP_instance.h"
#include "VRP_solution.h"
#include "VRP_scenario.h"
#include "NM.h"
#include "LS_Program.h"
#include "tools.h"
#include <cmath>
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */
#include <getopt.h>



bool VRP_request::operator<(const VRP_request& other) const {
	return id_instance_file < other.id_instance_file;
}

VRP_ScenarioPool<Scenario_SS_DS_VRPTW_CR, Solution_DS_VRPTW> *scenarioPool;

double evaluation_callback_GSA(Solution_DS_VRPTW& solution) {
	
	if (solution.getNumberViolations())
		return 10000 + solution.getCost() + 10000 * solution.getWeightViolations() + 1000000 * solution.getNumberViolations();

	double eval = scenarioPool->evalSolution_GSA(solution);

	// cout << "Eval callback called - Eval : " << eval << solution.toString() << endl;
	return eval;
}

int nb_iter = 0;
CallbackReturnType enditer_callback(Solution_DS_VRPTW& bestSolution, double &best_eval, Solution_DS_VRPTW& incumbentSolution, double &incumbent_eval) {
	nb_iter++;
	if (nb_iter == 200) {
		cout << endl << "RESAMPLING POOL" << endl;
		scenarioPool->reSamplePool(0);
		best_eval  = scenarioPool->evalSolution_GSA(bestSolution);
		incumbentSolution = bestSolution;
		incumbent_eval = best_eval;
		nb_iter = 0;
	}
	return NONE;
}

int main(int argc, char **argv) {

	const char* instance_file = nullptr;
	string instance_type;
	int verbose = false;

	static struct option long_options[] = {
		{"verbose"		, no_argument		, &verbose,	1},
		{"help"			, no_argument		, 0, 'h'},
		{"instance-type", required_argument	, 0, 't'},
		{"instance-file", required_argument	, 0, 'i'},
		{0, 0, 0, 0}
	};
	int c, option_index = 0;
	while ((c = getopt_long (argc, argv, "ht:i:", long_options, &option_index)) != -1 ) {
		switch (c) {
			case 'h': cout << "Usage: test_DSVRPTW [--help] [--verbose] --instance-type TYPE{BENT|...} --instance-file FILE" << endl; return 1;
			case 't': instance_type.assign(optarg); break;
			case 'i': instance_file = optarg; break;
			case '?': abort();
		}
	}
	if (instance_file == nullptr) cout << "ERROR: option --instance-file must be set" << endl, abort();
	if (instance_type != "BENT" && instance_type != "RIZZO") cout << "ERROR: option --type-instance must be set to either BENT or RIZZO" << endl, abort();
	


	srand(time(NULL));

	int scenario_number = 1;

	VRP_instance * I = nullptr;
	if (instance_type == "BENT") 				I = & VRP_instance_file::readInstanceFile_DS_VRPTW__Bent_PVH(instance_file);
	else if (instance_type == "RIZZO") 			I = & VRP_instance_file::readInstanceFile_DS_VRPTW__Rizzo(instance_file, scenario_number, false);
	else _ASSERT_(false, "wrong instance type " << instance_type);

	cout << "Instance file: " << instance_file << endl;
	
	// cout << I->toString() << endl;
	// cout << I->toStringCoords(true) << endl;
	// cout << I->toString_TravelTimes() << endl;
	// cout << I->toStringProbasTS() << endl << endl;
	// cout << I->toStringInstanceMetrics() << endl;

	cout << I->toString_AppearingOnlineRequests() << endl;
	cout << I->toStringVehicleTypes() << endl;
	cout << I->toStringPotentialOnlineRequests() << endl;


	Solution_DS_VRPTW s(*I);
	const VRP_VehicleType* van;
	const VRP_VehicleType* bicycle;
	for (const VRP_VehicleType* veh_type : I->getVehicleTypes()) {
		if (veh_type->getName() == "Van") van = veh_type;
		else if (veh_type->getName() == "Bicycle") bicycle = veh_type;
		else _ASSERT_(false, "");
	}
	for (const VRP_vertex* depot : I->getDepotVertices()) {
		if (depot->getIdInstance() == 1) s.addRoute(*depot, *van, 3);
		else if (depot->getIdInstance() == 2) s.addRoute(*depot, *bicycle, 5);
		else _ASSERT_(false, "");
	}
	// s.setWaitingStrategy(DRIVE_FIRST_DELAY_LAST);
	// s.setWaitingStrategy(WAIT_FIRST);
	s.setWaitingStrategy(RELOCATION_ONLY);
	s.generateInitialSolution();		// here all the requests with prebin probability > 0 will be inserted
	cout << "Initial solution:" << endl << s.toString(false);
	// cout << s.toString(true) << endl;


}




