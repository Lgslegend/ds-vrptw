/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/



#include "VRP_instance.h"
#include "VRP_solution.h"
#include "VRP_scenario.h"
#include "NM.h"
#include "LS_Program.h"
#include "tools.h"
#include <cmath>
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */
#include <getopt.h>


float offline_computation_time = -1;
float sec_per_time_unit = -1;
int pool_size = 20, refresh_rate = 50;

int nb_accepts = 0, nb_rejects = 0;

int nb_iter_feasible = 0;
double current_time_unit = 0;
double elapsed_real_time = 0.0, current_expected_realtime = 0.0, elapsed_nb_iter = 0.0;
clock_t t_nb_iter;
bool greedy = false;

const char* current_solution_file = nullptr;
ofstream current_solution_file_stream;
// output current solution
// CallbackReturnType new_best_callback(Solution_DS_VRPTW& bestSolution, double& best_eval) {
CallbackReturnType time_callback(Solution_DS_VRPTW& bestSolution, double &best_eval, Solution_DS_VRPTW& incumbentSolution, double &incumbent_eval, double elapsed_time) {
	UNUSED(elapsed_time); UNUSED(incumbent_eval); UNUSED(incumbentSolution);
	elapsed_nb_iter = (double)(clock() - t_nb_iter) / CLOCKS_PER_SEC; 
	t_nb_iter = clock();
	double iter_per_sec = nb_iter_feasible / elapsed_nb_iter;
	nb_iter_feasible = 0; 

	current_solution_file_stream.open(current_solution_file, ios::out | ios::trunc); 
	if (!greedy) {
		current_solution_file_stream << "Pool size: " << setw(5) << pool_size << "     Re-sampling rate: " << setw(5) << refresh_rate;
		current_solution_file_stream << "     #it/s: " << setw(10) << iter_per_sec << endl;
	}
	// else current_solution_file_stream << endl;
	current_solution_file_stream << " #accepts: " << outputMisc::boolColorExpr(true) << setw(5) << nb_accepts << outputMisc::resetColor() << "             #rejects: " << outputMisc::boolColorExpr(false) << setw(5) << nb_rejects << outputMisc::resetColor();
	current_solution_file_stream << "     Time unit: " << setw(6) << current_time_unit << "     Elapsed time: " << setw(5) << elapsed_real_time  << endl << endl;
	current_solution_file_stream << "Evaluation: " << best_eval << " \t(cost: " << bestSolution.getCost() << ")";
	int n_viol = bestSolution.getNumberViolations();
	if (n_viol) current_solution_file_stream << "   #violations: " << n_viol;
	current_solution_file_stream << endl << bestSolution.toString();
	current_solution_file_stream << flush;
	current_solution_file_stream.close();
	return NONE;
}


bool VRP_request::operator<(const VRP_request& other) const {
	return id_instance_file < other.id_instance_file;
}


VRP_ScenarioPool<Scenario_SS_DS_VRPTW_CR, Solution_DS_VRPTW> *scenarioPool;

double evaluation_callback_GSA(Solution_DS_VRPTW& solution) {
	
	if (solution.getNumberViolations()) {
		double eval = 10000 + solution.getCost() + 10000 * solution.getWeightViolations() + 1000000 * solution.getNumberViolations();
		// cout << "Eval callback called on infeasible solution - Eval : " << eval << endl << solution.toString() << endl;
		return eval;
	}
	double eval = 0.000001 * solution.getCost() + scenarioPool->evalSolution_GSA(solution);
	// double eval = scenarioPool->evalSolution_GSA(solution);

	return eval;
}

// in first offline optimization, we stop at soon as we get a feasible solution
CallbackReturnType enditer_callback_offline(Solution_DS_VRPTW& bestSolution, double &best_eval, Solution_DS_VRPTW& incumbentSolution, double &incumbent_eval) {
	UNUSED(best_eval); UNUSED(incumbent_eval); UNUSED(incumbentSolution);
	if (bestSolution.getNumberViolations() == 0) return STOP_LS;
	return NONE;
}

// Pool refresh
CallbackReturnType enditer_callback(Solution_DS_VRPTW& bestSolution, double &best_eval, Solution_DS_VRPTW& incumbentSolution, double &incumbent_eval) {
	if (bestSolution.getNumberViolations()) return NONE;
	if (nb_iter_feasible % refresh_rate == 0) {
		// cout << endl << "RESAMPLING POOL" << endl;
		scenarioPool->reSamplePool(current_time_unit);
		best_eval  = scenarioPool->evalSolution_GSA(bestSolution);
		incumbentSolution = bestSolution;
		incumbent_eval = best_eval;
	}
	nb_iter_feasible++;
	return NONE;
}

void help() {
	cout << "Usage: test_DSVRPTW [--help] " << endl; 
	cout << "Usage: test_DSVRPTW [--verbose(+,++)] -t TYPE{BENT | RIZZO} -i FILE -o OTIME -s SEC [-O FILE] [–p SIZE=20] [-R RATE=50] [-G]" << endl; 
	cout << "Usage: test_DSVRPTW [--verbose(+,++)] --type-instance TYPE{BENT|...} --instance-file FILE --offline-computation-time OTIME --sec-per-tu SEC [--output-current-sol FILE] [--pool-size SIZE="<<pool_size<<"] [--refresh-rate RATE="<<refresh_rate<<"] [--greedy]" << endl; 
}

int main(int argc, char **argv) {

	if (argc < 2) { help(); return 1; }

	const char* instance_file = nullptr;
	string instance_type;
	int verbose = 0;
	int scenario_number = -1;
	int nb_vans = 0, nb_bikes = 0;
	int depot_vans = 0, depot_bikes = 0;
	int dropboxes = -1;

	static struct option long_options[] = {
		{"verbose"		, no_argument		, &verbose,	1},
		{"verbose+"		, no_argument		, &verbose,	2},
		{"verbose++"	, no_argument		, &verbose,	3},
		{"help"			, no_argument		, 0, 'h'},
		{"type-instance", required_argument	, 0, 't'},
		{"instance-file", required_argument	, 0, 'i'},
		{"offline-computation-time", required_argument	, 0, 'o'},
		{"sec-per-tu"	, required_argument	, 0, 's'},
		{"output-current-sol"	, required_argument	, 0, 'O'},
		{"pool-size"	, required_argument	, 0, 'p'},
		{"refresh-rate"	, required_argument	, 0, 'R'},
		{"scenario"		, required_argument, 0, 'S'},
		{"greedy"		, no_argument		, 0, 'G'},
		{"vehicles-type", required_argument	, 0, 'V'},
		{"dropboxes"	, required_argument	, 0, 'D'},
		{0, 0, 0, 0}
	};
	int c, option_index = 0;
	while ((c = getopt_long (argc, argv, "ht:i:o:s:O:p:R:S:GV:D:", long_options, &option_index)) != -1 ) {
		vector<string> x;
		switch (c) {
			case 'h': help(); return 1;
			case 't': instance_type.assign(optarg); break;
			case 'i': instance_file = optarg; break;
			case 'o': offline_computation_time = atof(optarg); break;
			case 's': sec_per_time_unit = atof(optarg); break;
			case 'O': current_solution_file = optarg; break;
			case 'p': pool_size = atoi(optarg); break;
			case 'R': refresh_rate = atoi(optarg); break;
			case 'S': scenario_number = atoi(optarg); break;
			case 'G': greedy = true; break;
			case 'V': 
				x = split<string>(string(optarg), ':');
				if (x[0] == "van") { _ASSERT_(nb_vans == 0, "Vehicle type van already set !"); nb_vans = stoi(x[2]); depot_vans = stoi(x[1]); } 
				if (x[0] == "bike") { _ASSERT_(nb_bikes == 0, "Vehicle type bike already set !"); nb_bikes = stoi(x[2]); depot_bikes = stoi(x[1]); } 
				break;
			case 'D': dropboxes = atoi(optarg); break;
			case '?': abort();
		}
	}
	if (instance_type != "BENT" && instance_type != "RIZZO") cout << "ERROR: option --type-instance must be set to either BENT or RIZZO (if RIZZO, the path to the different files must be provided with --instance-file, along with the scenario number to be simulated, with --scenario NUMBER)" << endl, abort();
	if (instance_type == "RIZZO") {
		if (scenario_number == -1) cout << "ERROR: if instance type is set to RIZZO, the scenario number to be simulated must be specified with --scenario NUMBER" << endl, abort();
		if (dropboxes != 0 && dropboxes != 1) cout << "ERROR: if instance type is set to RIZZO, you must specify whether dropboxes are used or not with --dropboxes (or -D) and value 0 or 1" << endl, abort();
		if (nb_vans + nb_bikes <= 0) cout << "ERROR: if instance type is set to RIZZO, the scenario number to be simulated must be specified with --vehicle-type (or -V) VNAME:DEPOT:NVEH; e.g. --vehicle-type van:1:5 means that 5 vehicles of type named \"van\" in the instance files are assigned to depot number 1" << endl, abort();
	}
	if (instance_file == nullptr) cout << "ERROR: option --instance-file must be set" << endl, abort();
	if (offline_computation_time < 0) cout << "ERROR: option --offline-computation-time (-o) must be set" << endl, abort();
	if (sec_per_time_unit < 0) cout << "ERROR: option --sec-per-tu (-s, #seconds per time unit) must be set" << endl, abort();
	if (sec_per_time_unit < 0.5) cout << "ERROR: option --sec-per-tu (-s, #seconds per time unit) must be at least 0.5 (given: " << sec_per_time_unit << ")" << endl, abort();

	srand(time(NULL));

	VRP_instance * I = nullptr;
	if (instance_type == "BENT") 				I = & VRP_instance_file::readInstanceFile_DS_VRPTW__Bent_PVH(instance_file);
	else if (instance_type == "RIZZO") 			I = & VRP_instance_file::readInstanceFile_DS_VRPTW__Rizzo(instance_file, scenario_number, dropboxes);
	else _ASSERT_(false, "wrong instance type " << instance_type);


	cout << "Instance file: " << instance_file;
	
	cout << I->toString() << endl;
	// cout << I->toStringCoords(true) << endl;
	// cout << I->toString_TravelTimes() << endl;
	// cout << I->toStringProbasTS() << endl << endl;
	// cout << I->toStringInstanceMetrics() << endl;


	Solution_DS_VRPTW s(*I);
	// s.setWaitingStrategy(DRIVE_FIRST);
	// s.setWaitingStrategy(DRIVE_FIRST_DELAY_LAST);
	s.setWaitingStrategy(RELOCATION_ONLY);

	if (instance_type == "BENT")
		s.addRoutes(I->_getNumberVehicles());

	if (instance_type == "RIZZO") {
		_ASSERT_(depot_vans <= 2 && depot_bikes <= 2, "Error: Depot numbers are: 1, 2");
		const VRP_VehicleType& van = I->getVehicleType("van");
		const VRP_VehicleType& bike = I->getVehicleType("bike");
		const VRP_vertex* depots[3];
		depots[1] = I->getVertexFromInstanceFileID(VRP_vertex::DEPOT, 1);
		depots[2] = I->getVertexFromInstanceFileID(VRP_vertex::DEPOT, 2);

		if (nb_vans) s.addRoute(*depots[depot_vans], van, nb_vans);
		for (int i=0; i < nb_bikes; i++) {
			int route = s.addRoute(*depots[depot_bikes], bike);
			s.activateAutomaticVehicleUnload(route);
		}
	}

	
	s.generateInitialSolution();		// here all the requests with prebin probability > 0 will be inserted
	cout << "Initial solution:" << endl << s.toString(verbose >= 3) << endl;

	// Neighborhood manager & LS Program
	NeighborhoodManager<Solution_DS_VRPTW> nm_gsa(s, DS_VRPTW_GSA);
	NeighborhoodManager<Solution_DS_VRPTW> nm_vrptw(s, VRPTW);
	LS_Program_Basic<Solution_DS_VRPTW,NeighborhoodManager<Solution_DS_VRPTW>> lsprog_gsa;
	LS_Program_SimulatedAnnealing<Solution_DS_VRPTW,NeighborhoodManager<Solution_DS_VRPTW>> lsprog;


	// Scenario pool
	scenarioPool = new VRP_ScenarioPool<Scenario_SS_DS_VRPTW_CR, Solution_DS_VRPTW>(*I, pool_size, DS_VRPTW, 0);

	clock_t t = clock(); t_nb_iter = clock();

	// OFFLINE COMPUTATION ##################################################################################################################
	if (s.getNumberViolations()) {
		cout << "Offline computation: looking for feasible solution (max " << offline_computation_time << "s) ..." << flush;
		lsprog.setTimeOut(offline_computation_time);
		lsprog.set_EndOfIteration_Callback(enditer_callback_offline);
		if (current_solution_file != nullptr) lsprog.set_Time_Callback(time_callback, 0.5);
		lsprog.run(s, nm_vrptw, numeric_limits<int>::max(), 5.0, 0.995, verbose >= 2);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
		if (!s.getNumberViolations())
			cout << "   Found !" << endl;
	}
	if (s.getNumberViolations()) {
		cout << "No initial feasible solution found ! Abording." << endl;
		abort();
	} 
	elapsed_real_time = (double)(clock() - t) / CLOCKS_PER_SEC;
	if (verbose >= 2) cout << s.toString(verbose >= 3);

	cout << "Offline computation: searching for anticipative actions (" << offline_computation_time - elapsed_real_time << "s) ..." << flush;
	s.setCurrentTime(0);
	lsprog_gsa.setTimeOut(offline_computation_time - elapsed_real_time);
	if (!greedy) {
		lsprog_gsa.set_Evaluation_Callback(evaluation_callback_GSA);
		lsprog_gsa.set_EndOfIteration_Callback(enditer_callback);
	}
	if (current_solution_file != nullptr) lsprog_gsa.set_Time_Callback(time_callback, 0.5);
	lsprog_gsa.run(s, nm_gsa, numeric_limits<int>::max(), 0, numeric_limits<int>::max(), verbose >= 2);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
	cout << "   Done." << endl;
	cout << "New initial solution:" << endl << s.toString(verbose >= 3);

	// ONLINE PART ##########################################################################################################################
	// cout << "Real scenario:" << endl << I->toString_AppearingOnlineRequests() << endl;

	current_time_unit = 1.0;
	elapsed_real_time = 0.0; 
	current_expected_realtime = 0.0;
	t = clock();

	cout << endl << "Starting simulation." << endl;

	if (verbose) cout << fixed << "Time Unit    CPU Time    #rej.        Eval      Event(s)" << endl;
	else cout << "Current time unit: " << flush;

	s.setCurrentTime(1);
	auto it = I->getAppearingOnlineRequests().begin();
	while (current_time_unit <= I->getHorizon()) {
		current_expected_realtime = (current_time_unit-1) * sec_per_time_unit;
		elapsed_real_time = (double)(clock() - t) / CLOCKS_PER_SEC;
		_ASSERT_(elapsed_real_time - current_expected_realtime < sec_per_time_unit * 3/4 , "");				// is the system answering fast enough to potential online requests ? 
		if (verbose) cout << endl << setw(9) << setprecision(1) << current_time_unit << setw(12)  << setprecision(2) << elapsed_real_time << setw(9) << nb_rejects << setw(12) << setprecision(2) << s.getCost() << "      "; 											
		else if (current_time_unit == floor(current_time_unit)) cout << setw(5) << current_time_unit << flush;	
		if (it != I->getAppearingOnlineRequests().end()) {				// is there still incoming online requests ?
			
			/********************************************** Online requests management phase ***************************************/

			// Are there some online requests at the current time unit ?
			if (verbose) cout << "Online requests:  ";
			while (it != I->getAppearingOnlineRequests().end() && it->reveal_time == current_time_unit) {
				const VRP_request& r = * it->request;
				I->set_OnlineRequest_asAppeared(r);		// inform the instance object that the request is from now marked as appeared
				bool success = s.tryInsertOnlineRequest(r);
				if (! success) nb_rejects++; else nb_accepts++;
				if (verbose) cout << outputMisc::boolColorExpr(success) << r.toString() << outputMisc::resetColor() << "  ";
				it++;
			}


			/********************************************** Optimization phase *****************************************************/
			elapsed_real_time = (double)(clock() - t) / CLOCKS_PER_SEC;
			double computation_time = (current_expected_realtime + (0.5 * sec_per_time_unit)) - elapsed_real_time;
			computation_time = max(computation_time - 0.1, 0.01);
			_ASSERT_(computation_time >= sec_per_time_unit / 10, "");	// is there enough remaining time to perform optimization ? 

			// ReSample scenario pool & Fix current solution for 1 time unit ahead
			s.setCurrentTime(current_time_unit + 1);
			if (verbose) cout << endl << setw(21)  << setprecision(2) << elapsed_real_time << setw(9) << nb_rejects << setw(12) << setprecision(2) << s.getCost() << "      Solution fixed until time unit " << current_time_unit + 1 << "; optimize for " << computation_time << "s; "; 											
			if (!greedy && current_time_unit == floor(current_time_unit)) {
				scenarioPool->reSamplePool(current_time_unit);
				if (verbose) cout << "(pool resampled from time unit " << current_time_unit+1 << ")";
			}
			// Optimize during a half time unit minus the time spent for handling online requests
			if (verbose >= 3) cout << endl;
			lsprog_gsa.setTimeOut(computation_time);
			lsprog_gsa.run(s, nm_gsa, numeric_limits<int>::max(), 0, numeric_limits<int>::max(), verbose >= 3);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
			

		} else {
			cout << endl << "No more online requests to arrive... Terminating." << endl;
			break;
		}

		current_time_unit += 0.5;
	}

	if (verbose) cout << endl << s.toString() << endl << endl;

	cout << endl << endl << "Number of rejected requests: \t" << nb_rejects << " / " << I->getAppearingOnlineRequests().size() << endl;
	cout << "Total traveled distances:" << endl;
	for (const VRP_VehicleType* veh : I->getVehicleTypes()) {
		double dist = s.getTraveledDistancePerVehicleType(*veh);
		cout << "    " << veh->toString() << ": \t" << dist << " km" << endl;
	}
	cout << endl;
	
}




