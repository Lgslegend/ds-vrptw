#include "VRP_instance.h"
#include "VRP_solution.h"
#include "VRP_scenario.h"
#include "NM.h"
#include "LS_Program.h"
#include "tools.h"
#include <queue>
#include <cmath>
#include <limits>
#include <getopt.h>


using namespace std;

bool VRP_request::operator<(const VRP_request& other) const {
	return id_instance_file < other.id_instance_file;
}

int main(int argc, char **argv) {
	const char* instance_file = nullptr;
	int verbose = false;

	static struct option long_options[] = {
		{"verbose"		, no_argument		, &verbose,	1},
		{"help"			, no_argument		, 0, 'h'},
		{"instance-file", required_argument	, 0, 'i'},
		{0, 0, 0, 0}
	};

	int c, option_index = 0;
	while ((c = getopt_long (argc, argv, "hi:", long_options, &option_index)) != -1 ) {
		switch (c) {
			case 'h': cout << "Usage: test_DSVRPTW [--help] [--verbose] --instance-file FILE" << endl; return 1;
			case 'i': instance_file = optarg; break;
			case '?': abort();
		}
	}
	if (instance_file == nullptr) cout << "ERROR: option --instance-file must be set" << endl, abort();






	// srand(time(NULL));
	srand(1000);

	// INSTANCE FILE
	VRP_instance& I = VRP_instance_file::readInstanceFile_DS_VRPTW__Bent_PVH(instance_file);
	cout << I.toString() << endl;
	// cout << I.toStringCoords(true) << endl;
	// cout << I.toStringDistances() << endl;
	// cout << I.toStringProbasTS() << endl << endl;



	// SOLUTION 
	Solution_DS_VRPTW s(I);
	// s.setWaitingStrategy(DRIVE_FIRST_DELAY_LAST);
	s.setWaitingStrategy(WAIT_FIRST);

	// NEIGHBORHOOD MANAGER
	NeighborhoodManager<Solution_DS_VRPTW> nm(s, DS_VRPTW);


	// MAIN
	s.generateInitialSolution();		// here all the requests with prebin probability > 0 will be inserted
	cout << "Initial solution:" << endl << s.toString(false) << endl;
	cout << "Cost initial solution: " << s.getCost()  << "   violations: " <<  s.getWeightViolations()<< endl;
	// cout << s.toString(true) << endl;
	
	cout << "LS ..." << endl;
	
	LS_Program_SimulatedAnnealing<Solution_DS_VRPTW,NeighborhoodManager<Solution_DS_VRPTW>> lsprog;
	// lsprog.setTimeOut(0.05);		
	// lsprog.run(s, nm, numeric_limits<int>::max(), 10.0, 0.95, true);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
	lsprog.run(s, nm, 10000, 5.0, 0.95, false);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)

	cout << s.toString(false) << endl;
	cout << "Cost: " << s.getCost()  << "   violations: " <<  s.getWeightViolations()<< endl;



	
	Scenario_SS_DS_VRPTW_CR scenario(I, DS_VRPTW, 0);


	cout << "Insert some waiting vertices" << endl;

	int i = 0;
	for (const VRP_instance::RequestAttributes& req_attr : scenario.getRequestSequence()) {
		const VRP_vertex& w = * I.getVertexFromRegion(VRP_vertex::WAITING, req_attr.vertex->getRegion());
		s.insertWaitingVertex(w, 1, 1);
		if(5 == i++) break;
	}

	cout << s.toString() << endl;
	s.setCurrentTime(50);
	lsprog.run(s, nm, 20000, 5.0, 0.95, false);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
	cout << s.toString() << endl;
	cout << s.toString(true) << endl;


}



