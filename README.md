# README #

This repository contains implementations of DS-VRPTW-CR solvers. 

Everything here is based on the [VRPlib library](https://bitbucket.org/mstguillain/vrplib).
The library contains the object oriented representations of a VRP solutions and its constraints, 
together with complex evaluation functions in case of Dyanmic and/or Stochastic extensions.

In particular, solutions for the DS-VRTPW (see **[1]**) can be represented, and their expected cost computed. 

The programs writen in this repository are simple constructions based on the VRPlib API. 


---


Copyright 2017 Michael Saint-Guillain.

The library and programs are under GNU Lesser General Public License (LGPL) v3.

**Contact**: Michael Saint-Guillain *<m dot stguillain at famous google mail dot com>*

---


**[1]** Saint-Guillain, M., Deville, Y., Solnon, C., 2015. *A Multistage Stochastic Programming 
Approach to the Dynamic and Stochastic VRPTW.* In: CPAIOR 2015. Springer International Publishing, pp. 357–374.    
[_Download link_](http://becool.info.ucl.ac.be/biblio/multistage-stochastic-programming-approach-dynamic-and-stochastic-vrptw)