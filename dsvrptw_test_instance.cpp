#include "VRP_instance.h"
#include "VRP_solution.h"
#include "VRP_scenario.h"
#include "NM.h"
#include "LS_Program.h"
#include "tools.h"
#include <cmath>
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */
#include <getopt.h>

#include <iostream>
#include <fstream>


int pool_size = 50, refresh_rate = 50;


bool VRP_request::operator<(const VRP_request& other) const {
	return id_instance_file < other.id_instance_file;
}

VRP_ScenarioPool<Scenario_SS_DS_VRPTW_CR, Solution_DS_VRPTW> *scenarioPool;


const char* current_solution_file = nullptr;
ofstream current_solution_file_stream;
CallbackReturnType new_best_callback(Solution_DS_VRPTW& bestSolution, double& best_eval) {
	current_solution_file_stream.open(current_solution_file, ios::out | ios::trunc); 
	current_solution_file_stream << endl << "Pool size: " << setw(5) << pool_size << "     Resample rate: " << setw(5) << refresh_rate << endl << endl;
	current_solution_file_stream << endl << "Evaluation: " << best_eval << endl << bestSolution.toString();
	current_solution_file_stream.close();
	return NONE;
}

double evaluation_callback_GSA(Solution_DS_VRPTW& solution) {
	
	if (solution.getNumberViolations())
		return 10000 + solution.getCost() + 10000 * solution.getWeightViolations() + 1000000 * solution.getNumberViolations();

	double eval = scenarioPool->evalSolution_GSA(solution);

	// cout << "Eval callback called - Eval : " << eval << solution.toString() << endl;
	return eval;
}

int nb_iter = 0;
CallbackReturnType enditer_callback(Solution_DS_VRPTW& bestSolution, double &best_eval, Solution_DS_VRPTW& incumbentSolution, double &incumbent_eval) {
	nb_iter++;
	if (nb_iter == refresh_rate) {
		cout << endl << "RESAMPLING POOL" << endl;
		scenarioPool->reSamplePool(0);
		best_eval  = scenarioPool->evalSolution_GSA(bestSolution);
		incumbentSolution = bestSolution;
		incumbent_eval = best_eval;
		nb_iter = 0;
	}
	return NONE;
}

int main(int argc, char **argv) {

	const char* instance_file = nullptr;
	string instance_type;
	int verbose = false;

	static struct option long_options[] = {
		{"verbose"		, no_argument		, &verbose,	1},
		{"help"			, no_argument		, 0, 'h'},
		{"type-instance", required_argument	, 0, 't'},
		{"instance-file", required_argument	, 0, 'i'},
		{"output-current-sol", required_argument	, 0, 'O'},
		{"pool-size"	, required_argument	, 0, 'p'},
		{"refresh-rate"	, required_argument	, 0, 'R'},
		{0, 0, 0, 0}
	};
	int c, option_index = 0;
	while ((c = getopt_long (argc, argv, "ht:i:c:O:p:R:", long_options, &option_index)) != -1 ) {
		switch (c) {
			case 'h': cout << "Usage: test_DSVRPTW [--help]" << endl; 
				cout << "Usage: test_DSVRPTW [--verbose(+,++)] -t TYPE{BENT|...} -i FILE [-O FILE] [–p SIZE=20] [-R RATE=50]" << endl; 
				cout << "Usage: test_DSVRPTW [--verbose(+,++)] --type-instance TYPE{BENT|...} --instance-file FILE [--pool-size SIZE=20] [--refresh-rate RATE=50]" << endl; 
				return 1;
			case 't': instance_type.assign(optarg); break;
			case 'i': instance_file = optarg; break;
			case 'O': current_solution_file = optarg; break;
			case 'p': pool_size = atoi(optarg); break;
			case 'R': refresh_rate = atoi(optarg); break;
			case '?': abort();
		}
	}
	if (instance_type != "BENT") cout << "ERROR: option --type-instance must be set to BENT" << endl, abort();
	if (instance_file == nullptr) cout << "ERROR: option --instance-file must be set" << endl, abort();

	
		


	srand(time(NULL));




	VRP_instance * I = nullptr;
	if (instance_type == "BENT") 				I = & VRP_instance_file::readInstanceFile_DS_VRPTW__Bent_PVH(instance_file);
	else _ASSERT_(false, "wrong instance type " << instance_type);

	cout << "Instance file: " << instance_file;
	
	cout << I->toString() << endl;
	// cout << I->toStringCoords(true) << endl;
	// cout << I->toString_TravelTimes() << endl;
	// cout << I->toStringProbasTS() << endl << endl;
	// cout << I->toStringInstanceMetrics() << endl;


	Solution_DS_VRPTW s(*I);
	// s.setWaitingStrategy(DRIVE_FIRST_DELAY_LAST);
	// s.setWaitingStrategy(WAIT_FIRST);
	s.setWaitingStrategy(RELOCATION_ONLY);
	s.generateInitialSolution();		// here all the requests with prebin probability > 0 will be inserted
	cout << "Initial solution:" << endl << s.toString(false);
	// cout << s.toString(true) << endl;

	// Neighborhood manager & LS Program
	NeighborhoodManager<Solution_DS_VRPTW> nm(s, DS_VRPTW);
	LS_Program_SimulatedAnnealing<Solution_DS_VRPTW,NeighborhoodManager<Solution_DS_VRPTW>> lsprog;
	lsprog.setTimeOut(5);
	if (current_solution_file != nullptr)
		lsprog.set_NewBestSolution_Callback(new_best_callback);
	lsprog.run(s, nm, numeric_limits<int>::max(), 5.0, 0.995, true);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
	cout << s.toString();
	// cout << s.toString(true);


	NeighborhoodManager<Solution_DS_VRPTW> nm_gsa(s, DS_VRPTW_GSA);
	LS_Program_Basic<Solution_DS_VRPTW,NeighborhoodManager<Solution_DS_VRPTW>> lsprog_gsa;
	// LS_Program_SimulatedAnnealing<Solution_DS_VRPTW,NeighborhoodManager<Solution_DS_VRPTW>> lsprog_gsa;

	scenarioPool = new VRP_ScenarioPool<Scenario_SS_DS_VRPTW_CR, Solution_DS_VRPTW>(*I, pool_size, DS_VRPTW, 0);
	// cout << scenarioPool.toString() << endl;
	s.setCurrentTime(0);
	// cout << "SAA = " << scenarioPool->evalSolution_GSA(s) << endl;
	// cout << "SAA = " << scenarioPool->evalSolution_GSA(s) << endl;
	// cout << "SAA = " << scenarioPool->evalSolution_GSA(s) << endl;

	lsprog_gsa.setTimeOut(1800);
	lsprog_gsa.set_Evaluation_Callback(evaluation_callback_GSA);
	lsprog_gsa.set_EndOfIteration_Callback(enditer_callback);
	if (current_solution_file != nullptr)
		lsprog_gsa.set_NewBestSolution_Callback(new_best_callback);
	// lsprog_gsa.run(s, nm, numeric_limits<int>::max(), 1.0, 0.99, true);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
	lsprog_gsa.run(s, nm_gsa, numeric_limits<int>::max(), 0, numeric_limits<int>::max(), true);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
	cout << s.toString();

}




